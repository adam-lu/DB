-- 1.数据库准备
CREATE database EP_analysis;
use EP_analysis;

-- 2.表准备
create table ep_trade_user
(
user_id int (9),
item_id int (9),
behavior_type int (1),
user_geohash varchar (14),
item_category int (5),
time varchar (13)
);

SELECT * FROM ep_trade_user;

-- 3.数据预处理
-- 3.1去重
CREATE TABLE temp_trade like ep_trade_user;
INSERT into temp_trade SELECT DISTINCT * FROM ep_trade_user;

-- 3.2 插入新字段date_time、date
alter table temp_trade add COLUMN date_time datetime null;
UPDATE temp_trade set date_time=STR_TO_DATE(time,"%Y-%m-%d %H");

alter table temp_trade add COLUMN date char(10) null;
UPDATE temp_trade set date=date(date_time);

-- 4. 用户体系建设
-- 4.1基础指标
-- uv、pv、浏览深度
SELECT 
	date,
	COUNT(DISTINCT user_id) uv,
	COUNT(IF(behavior_type=1,user_id,null)) pv,
	COUNT(IF(behavior_type=1,user_id,null))/COUNT(DISTINCT user_id) depth
FROM
	temp_trade	
GROUP BY
	date;
	
-- 留存率（次日、2日、3日、7日、15日、30日）
with remain_temp as
(SELECT
date1 date,
count(distinct if(date1=date2,user_id,null)) remain0,
count(distinct if(DATEDIFF(date2,date1)=1,user_id,null)) remain1,
count(distinct if(DATEDIFF(date2,date1)=2,user_id,null)) remain2,
count(distinct if(DATEDIFF(date2,date1)=3,user_id,null)) remain3,
count(distinct if(DATEDIFF(date2,date1)=7,user_id,null)) remain7,
count(distinct if(DATEDIFF(date2,date1)=15,user_id,null)) remain15,
count(distinct if(DATEDIFF(date2,date1)=30,user_id,null)) remain30
FROM
(SELECT t1.user_id user_id,
t1.date date1,
t2.date date2
FROM
(SELECT user_id,date FROM temp_trade GROUP BY user_id,date)t1
join
(SELECT user_id,date FROM temp_trade GROUP BY user_id,date)t2
on t1.user_id=t2.user_id
where t1.date<=t2.date
)t
GROUP BY date1)
SELECT date,
CONCAT(ROUND(remain1/remain0*100,2),'%') '次日留存率',
CONCAT(ROUND(remain2/remain0*100,2),'%') '二日留存率',
CONCAT(ROUND(remain3/remain0*100,2),'%') '三日留存率',
CONCAT(ROUND(remain7/remain0*100,2),'%') '七日留存率',
CONCAT(ROUND(remain15/remain0*100,2),'%') '15日留存率',
CONCAT(ROUND(remain30/remain0*100,2),'%') '30日留存率'
FROM remain_temp
GROUP BY date;

-- 5. RFM模型
-- 5.1 R部分
-- 创建Recency视图
CREATE VIEW user_recency
as
SELECT 
user_id,max(date) rec_buy_time
FROM
temp_trade
where behavior_type=2
GROUP BY user_id
ORDER BY rec_buy_time DESC;

SELECT * FROM user_recency;

-- 依据最近一次购买时间对用户进行分级
CREATE view r_level
as
SELECT user_id,
(case 
when DATEDIFF('2019-12-18',rec_buy_time)<=2 then 5
when DATEDIFF('2019-12-18',rec_buy_time)<=4 then 4
when DATEDIFF('2019-12-18',rec_buy_time)<=6 then 3
when DATEDIFF('2019-12-18',rec_buy_time)<=8 then 2
else 1
end
) r_value
FROM user_recency
GROUP BY user_id;

SELECT * FROM r_level;

-- 5.2 F部分
-- 创建Frequency视图
CREATE VIEW user_frequency
as
SELECT 
user_id,count(user_id) buy_frequency
FROM
temp_trade
where behavior_type=2
GROUP BY user_id
ORDER BY buy_frequency DESC;

SELECT * FROM user_frequency;
-- 依据频率对用户进行分级
CREATE view f_level
as SELECT
user_id,
(CASE WHEN buy_frequency<=2 THEN 1
WHEN buy_frequency<=4 THEN 2
WHEN buy_frequency<=6 THEN 3
WHEN buy_frequency<=8 THEN 4
ELSE 5
END) f_value
FROM user_frequency
GROUP BY user_id;

SELECT * FROM f_level;
-- 5.3 结果整合

-- r_value平均值
SELECT AVG(r_value)FROM r_level; -- 2.7939
-- f_value平均值
SELECT AVG(f_value)FROM f_level; -- 2.2606

-- 客户分类
create view id_category as
SELECT 
f.user_id user_id,
(case when r_value>=2.7939 and f_value>=2.2606 then '重要高价值客户'
when r_value<2.7939 and f_value>=2.2606 then '重要唤回客户'
when r_value>=2.7939 and f_value<2.2606 then '重要深耕客户'
else '重要挽留客户' end) category
from
f_level f join r_level r on f.user_id=r.user_id 
GROUP BY f.user_id;

SELECT * FROM id_category;

-- 6.商品体系建设
-- 6.1 商品
-- 商品的点击量 收藏量 加购量 购买次数 购买转化
select item_id,
sum(case when behavior_type=1 then 1 else 0 end) as'pv',
sum(case when behavior_type=4 then 1 else 0 end) as'fav',
sum(case when behavior_type=3 then 1 else 0 end) as'cart',
sum(case when behavior_type=2 then 1 else 0 end) as'buy',
count(distinct case when behavior_type=2 then user_id else null
end)/count(distinct user_id) as buy_rate
from temp_trade
group by item_id
order by buy desc;


-- 6.2 品类
select item_category,
sum(case when behavior_type=1 then 1 else 0 end) as'pv',
sum(case when behavior_type=4 then 1 else 0 end) as'fav',
sum(case when behavior_type=3 then 1 else 0 end) as'cart',
sum(case when behavior_type=2 then 1 else 0 end) as'buy',
count(distinct case when behavior_type=2 then user_id else null
end)/count(distinct user_id) as buy_rate
from temp_trade
group by item_category
order by buy desc;

-- 7.商品体系建设
-- 7.1、行为指标
-- 每日的分析(1-4，分别表示点击pv、购买buy、加购物车cart、喜欢fav)
select date,count(*),
sum(case when behavior_type=1 then 1 else 0 end) as'pv',
sum(case when behavior_type=4 then 1 else 0 end) as'fav',
sum(case when behavior_type=3 then 1 else 0 end) as'cart',
sum(case when behavior_type=2 then 1 else 0 end) as'buy',
count(distinct case when behavior_type=2 then user_id else null
end)/count(distinct user_id) as buy_rate
from temp_trade
group by date
order by buy desc;

-- 7.2 行为路径分析
-- 核心：拼接行为路径

-- 用户行为拼接准备
CREATE view user_act as
(SELECT * FROM
(SELECT user_id,item_id,
(LAG(behavior_type,4) over(PARTITION by user_id,item_id ORDER BY date_time)) lag4,
(LAG(behavior_type,3) over(PARTITION by user_id,item_id ORDER BY date_time)) lag3,
(LAG(behavior_type,2) over(PARTITION by user_id,item_id ORDER BY date_time)) lag2,
(LAG(behavior_type,1) over(PARTITION by user_id,item_id ORDER BY date_time)) lag1,
behavior_type,
RANK() over(PARTITION by user_id,item_id ORDER BY date_time desc) rank_number
FROM
temp_trade)a
where rank_number=1 and behavior_type=2);

-- 拼接行为路径
SELECT * FROM user_act;

SELECT user_id,item_id,
CONCAT(IFNULL(lag4,'空'),'-',IFNULL(lag3,'空'),'-',IFNULL(lag2,'空'),'-',IFNULL(lag1,'空'),'-',behavior_type) act_way
FROM user_act;

-- 针对行为路径进行统计
SELECT
act_way,
COUNT(distinct user_id) user_count
FROM
(SELECT user_id,item_id,
CONCAT(IFNULL(lag4,'空'),'-',IFNULL(lag3,'空'),'-',IFNULL(lag2,'空'),'-',IFNULL(lag1,'空'),'-',behavior_type) act_way
FROM user_act)t
GROUP BY act_way;